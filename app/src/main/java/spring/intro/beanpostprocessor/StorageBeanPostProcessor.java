package spring.intro.beanpostprocessor;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.util.StringUtils;
import spring.intro.dao.Storage;

import java.io.File;
import java.io.IOException;

public class StorageBeanPostProcessor implements BeanPostProcessor {
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_YELLOW = "\u001B[33m";
    private final Logger logger = LoggerFactory.getLogger(StorageBeanPostProcessor.class);
    private String preparedDataFilePath;

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof Storage && StringUtils.hasText(preparedDataFilePath)) {
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                Storage storage = objectMapper.readValue(new File(preparedDataFilePath), Storage.class);
                logger.info(ANSI_YELLOW + "Storage has been successfully initialized from file {}" + ANSI_RESET, preparedDataFilePath);
                return BeanPostProcessor.super.postProcessBeforeInitialization(storage, beanName);
            } catch (IOException e) {
                String errorText = String.format("Failed to initialize storage from file %s", preparedDataFilePath);
                logger.error(ANSI_YELLOW + errorText + ANSI_RESET, new BeanInitializationException(errorText));
            }
        }
        return BeanPostProcessor.super.postProcessBeforeInitialization(bean, beanName);
    }

    public void setPreparedDataFilePath(String preparedDataFilePath) {
        this.preparedDataFilePath = preparedDataFilePath;
    }
}
