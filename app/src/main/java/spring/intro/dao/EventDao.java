package spring.intro.dao;

import spring.intro.model.Event;
import spring.intro.model.EventImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class EventDao {

    private final ArrayList<EventImpl> data;

    public EventDao(Storage storage) {
        this.data = (ArrayList<EventImpl>) storage.getEvents();
    }

    public Event get(long id) {
        return data.stream()
                .filter(event -> event.getId() == id)
                .findFirst()
                .orElse(null);
    }

    public List<Event> get(String title, int pageSize, int pageNum) {
        return data.stream()
                .filter(event -> event.getTitle().contains(title))
                .skip(pageNum)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public List<Event> get(Date day, int pageSize, int pageNum) {
        return data.stream()
                .filter(event -> event.getDate().equals(day))
                .skip(pageNum)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public Event create(Event event) {
        EventImpl newEvent = (EventImpl) event;
        newEvent.setId(1 + (long) data.size());
        data.add((EventImpl) event);
        return newEvent;
    }

    public Event update(Event event) {
        Event eventToUpdate = data.stream()
                .filter(e -> e.getId() == event.getId())
                .findFirst()
                .orElseThrow();
        eventToUpdate.setDate(event.getDate());
        eventToUpdate.setTitle(event.getTitle());
        return eventToUpdate;
    }

    public boolean delete(long eventId) {
        boolean removed = data.removeIf(event -> event.getId() == eventId);
        if (removed) {
            data.trimToSize();
        }
        return removed;
    }
}
