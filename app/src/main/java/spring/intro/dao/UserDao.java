package spring.intro.dao;

import spring.intro.model.User;
import spring.intro.model.UserImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserDao {

    private final ArrayList<UserImpl> data;

    public UserDao(Storage storage) {
        this.data = (ArrayList<UserImpl>) storage.getUsers();
    }

    public User get(long id) {
        return data.stream()
                .filter(user -> user.getId() == id)
                .findFirst()
                .orElse(null);
    }

    public User get(String email) {
        return data.stream()
                .filter(user -> user.getEmail().equals(email))
                .findFirst()
                .orElse(null);
    }

    public List<User> get(String name, int pageSize, int pageNum) {
        return data.stream()
                .filter(user -> user.getName().contains(name))
                .skip(pageNum)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public User create(User user) {
        if (data.stream().anyMatch(u -> u.getEmail().equals(user.getEmail()))) {
            throw new IllegalArgumentException("User with " + user.getEmail() + " email already exists");
        }
        UserImpl newUser = (UserImpl) user;
        newUser.setId(1 + (long) data.size());
        data.add(newUser);
        return newUser;
    }

    public User update(User user) {
        UserImpl userToUpdate = data.stream()
                .filter(u -> u.getId() == user.getId())
                .findFirst()
                .orElseThrow();
        if (data.stream().anyMatch(u -> u.getId() != user.getId() && u.getEmail().equals(user.getEmail()))) {
            throw new IllegalArgumentException("User with " + user.getEmail() + " email already exists");
        }
        userToUpdate.setName(user.getName());
        userToUpdate.setEmail(user.getEmail());
        return userToUpdate;
    }

    public boolean delete(long userId) {
        boolean removed = data.removeIf(u -> u.getId() == userId);
        if (removed) {
            data.trimToSize();
        }
        return removed;
    }
}
