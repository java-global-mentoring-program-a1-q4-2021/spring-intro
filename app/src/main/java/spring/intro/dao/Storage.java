package spring.intro.dao;

import spring.intro.model.EventImpl;
import spring.intro.model.TicketImpl;
import spring.intro.model.UserImpl;

import java.util.ArrayList;
import java.util.List;

public class Storage {
    private List<UserImpl> users = new ArrayList<>();
    private List<EventImpl> events = new ArrayList<>();
    private List<TicketImpl> tickets = new ArrayList<>();

    public List<UserImpl> getUsers() {
        return users;
    }

    public void setUsers(List<UserImpl> users) {
        this.users = users;
    }

    public List<EventImpl> getEvents() {
        return events;
    }

    public void setEvents(List<EventImpl> events) {
        this.events = events;
    }

    public List<TicketImpl> getTickets() {
        return tickets;
    }

    public void setTickets(List<TicketImpl> tickets) {
        this.tickets = tickets;
    }
}
