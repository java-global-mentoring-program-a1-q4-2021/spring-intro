package spring.intro.service;

import org.springframework.beans.factory.annotation.Autowired;
import spring.intro.dao.UserDao;
import spring.intro.model.User;

import java.util.List;

public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    @Override
    public User getUserById(long userId) {
        return userDao.get(userId);
    }

    @Override
    public User getUserByEmail(String email) {
        return userDao.get(email);
    }

    @Override
    public List<User> getUsersByName(String name, int pageSize, int pageNum) {
        return userDao.get(name, pageSize, pageNum);
    }

    @Override
    public User createUser(User user) {
        return userDao.create(user);
    }

    @Override
    public User updateUser(User user) {
        return userDao.update(user);
    }

    @Override
    public boolean deleteUser(long userId) {
        return userDao.delete(userId);
    }
}
