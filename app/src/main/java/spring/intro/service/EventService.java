package spring.intro.service;

import spring.intro.model.Event;

import java.util.Date;
import java.util.List;

public interface EventService {
    Event getOneById(long eventId);
    List<Event> getByTitle(String title, int pageSize, int pageNum);
    List<Event> getByDate(Date day, int pageSize, int pageNum);
    Event create(Event event);
    Event update(Event event);
    boolean delete(long eventId);
}
