package spring.intro.service;

import org.springframework.beans.factory.annotation.Autowired;
import spring.intro.dao.EventDao;
import spring.intro.model.Event;

import java.util.Date;
import java.util.List;

public class EventServiceImpl implements EventService {
    @Autowired
    private EventDao eventDao;

    @Override
    public Event getOneById(long eventId) {
        return eventDao.get(eventId);
    }

    @Override
    public List<Event> getByTitle(String title, int pageSize, int pageNum) {
        return eventDao.get(title, pageSize, pageNum);
    }

    @Override
    public List<Event> getByDate(Date day, int pageSize, int pageNum) {
        return eventDao.get(day, pageSize, pageNum);
    }

    @Override
    public Event create(Event event) {
        return eventDao.create(event);
    }

    @Override
    public Event update(Event event) {
        return eventDao.update(event);
    }

    @Override
    public boolean delete(long eventId) {
        return eventDao.delete(eventId);
    }
}
